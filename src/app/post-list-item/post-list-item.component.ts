import { PostService } from './../services/post.service';
import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../post';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() post: Post

  constructor(private router: Router, private postService: PostService) { }

  ngOnInit() {
  }

  onClickLoveIt(post: Post) {
    post.loveIts++
    this.postService.updateLoveIts(post, post.loveIts)
  }
  
  onClickDontLoveIt(post: Post) {
    post.loveIts--
    this.postService.updateLoveIts(post, post.loveIts)
  }

  onDelete(post: Post) {
    if(confirm("Are you sure you want to delete " + "'" + post.title + "' ?"))
      this.postService.deletePost(post)
  }
}
