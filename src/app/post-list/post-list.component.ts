import { PostService } from './../services/post.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Post } from '../post';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {
  postsArray: Post[]
  postsSubscription: Subscription
  
  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.getPosts()
    this.postsSubscription = this.postService.postsSubject.subscribe(
      (posts: Post[]) => {
        this.postsArray = posts
      }
    )
    this.postService.emitPosts()
  }
  
  ngOnDestroy(): void {
    this.postsSubscription.unsubscribe()
  }
}
