import { Post } from "../post";
import { Subject } from 'rxjs';
import { HttpClient } from "../../../node_modules/@angular/common/http";
import { Injectable } from "../../../node_modules/@angular/core";
import * as firebase from 'firebase';

@Injectable()
export class PostService {
    posts: Post[] = []
    postsSubject = new Subject<Post[]>()

    constructor() { }

    getPosts() {
        firebase.database().ref('/posts')
            .on('value', (data: firebase.database.DataSnapshot) => {
                this.posts = data.val() ? data.val() : [];
                this.emitPosts();
            }
        );
    }

    savePosts() {
        firebase.database().ref('/posts').set(this.posts);
    }

    emitPosts() {
        this.posts = this.posts.sort((left, right): number => {
                if (left.title.toLowerCase() < right.title.toLowerCase())
                    return -1
                if (left.title.toLowerCase() > right.title.toLowerCase())
                    return 1
                return 0
            }
        )
        this.postsSubject.next(this.posts)
    }

    newPost(post: Post) {
        this.posts.push(post)
        this.savePosts()
        this.emitPosts()
    }

    private getPostIndex(post: Post) {
        return this.posts.findIndex(
            (element) => {
                if (element === post)
                    return true
            }
        )
    }

    deletePost(post: Post) {
        const index = this.getPostIndex(post)
        this.posts.splice(index, 1)
        this.savePosts()
        this.emitPosts()
    }

    updateLoveIts(post: Post, loveIts: number) {
        const index = this.getPostIndex(post)
        this.posts[index].loveIts = loveIts
        this.savePosts()
        this.emitPosts()
    }
}